#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>

#define DEVICE_NAME "loadcell"
#define CLASS_NAME "lc"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Truong Van Huy");
MODULE_DESCRIPTION("A loadcell driver");
MODULE_VERSION("0.1");

static int majorNumber;
static struct class* lccharClass = NULL;
static struct device* lccharDevice = NULL;
static char lcString[10] = {0};

static int	dev_open(struct inode *, struct file *);
static int	dev_release(struct inode *, struct file *);
static ssize_t	dev_read(struct file *, char *, size_t, loff_t *);

static struct file_operations fops =
{
	.open = dev_open,
	.read = dev_read,
	.release = dev_release,
};

static int __init lc_init(void) {
	int result = 0;
	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
	if (majorNumber < 0) {
		printk(KERN_ALERT "LC failed to register a major number\n");
		return majorNumber;
	}

	lccharClass = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(lccharClass)) {
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Failed to register device class\n");
		return PTR_ERR(lccharClass);
	}

	lccharDevice = device_create(lccharClass, NULL, MKDEV(majorNumber,0), NULL, DEVICE_NAME);
	if (IS_ERR(lccharDevice)) {
		class_destroy(lccharClass);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Failed to create device\n");
		return -ENODEV;
	}

	return 0;

}

static void __exit lc_exit(void) {
	device_destroy(lccharClass, MKDEV(majorNumber, 0));
	class_unregister(lccharClass);
	unregister_chrdev(majorNumber, DEVICE_NAME);

	printk(KERN_INFO "Load cell: goodbyte!\n");
}

static int dev_open(struct inode *inodep, struct file *filep) {
	return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset) {
	return 0;
}

static int dev_release(struct inode *inodep, struct file *filep) {
	return 0;
}

module_init(lc_init);
module_exit(lc_exit);
